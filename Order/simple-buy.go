package Order

type BuyOrder struct {
	Message string
}

func (state *BuyOrder) ID() string {
	return "Blyat"
}

func (state *BuyOrder) Owner() Wallet {

	wallet := &Wallet{}

	return *wallet
}

func (state *BuyOrder) Sell() bool {
	return true
}

func (state *BuyOrder) Price() Value {

	val := new(Value)

	return *val
}

func (state *BuyOrder) Quantity() Value {

	val := new(Value)

	return *val
}

func (state *BuyOrder) UpdateQuantity(val Value) {

}
