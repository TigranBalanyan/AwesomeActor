package main

import (
	"NairiCrypto/actors"

	"github.com/asynkron/protoactor-go/actor"
)

func main() {
	ctx := actor.NewActorSystem().Root
	props := actor.PropsFromProducer(func() actor.Actor { return &actors.OrderActor{} })
	pid := ctx.Spawn(props)

	balacePID := ctx.Spawn(actor.PropsFromProducer(func() actor.Actor { return &actors.BalanceActor{OrderActor: pid} }))
	ctx.Send(balacePID, "Fuck")
}
