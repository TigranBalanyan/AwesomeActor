package actors

import (
	"NairiCrypto/Order"
	"fmt"
	"time"

	"github.com/asynkron/protoactor-go/actor"
)

type OrderActor struct {
}

func (state *OrderActor) Receive(context actor.Context) {

	senderId := context.Sender()

	switch msg := context.Message().(type) {
	case Order.Order:

		context.Send(senderId, msg)
		fmt.Printf("Hello %v\n", msg.ID())

	}
}

type BalanceActor struct {
	OrderActor *actor.PID
}

func (state *BalanceActor) Receive(context actor.Context) {

	switch msg := context.Message().(type) {
	case Order.Order:

		fmt.Printf("World %v\n", msg.ID())
	case string:
		fmt.Println(msg)
		response := context.RequestFuture(state.OrderActor, Order.BuyOrder{Message: "FF"}, time.Duration(10000))
		response.PipeTo(context.Self())
	case actor.Future:
		fmt.Println(msg.Result())

	}
}
